#!/bin/bash

# store current path
coverage_path=$(pwd)
cd ..

# clone dumux modules
git clone https://git.iws.uni-stuttgart.de/dumux-repositories/dumux-testing.git
git clone https://git.iws.uni-stuttgart.de/dumux-repositories/dumux.git

# build dumux modules
for MOD in dumux dumux-testing dumux-coverage; do
    dunecontrol --builddir=/dumux-ci/build --opts=/dumux-ci/src/debug.opts --only=$MOD all
done

# build and run tests in dumux-testing
ninja -C /dumux-ci/build/dumux-testing -k 0 build_tests
cd /dumux-ci/build/dumux-testing
ctest -j8 --output-on-failure

# build and run tests in dumux
ninja -C /dumux-ci/build/dumux -k 0 build_tests
cd /dumux-ci/build/dumux
ctest -j8 --output-on-failure
# rerun the failed test non-concurrently so they hopefully do not timeout
ctest --rerun-failed

# run coverage in dumux-coverage
cd /dumux-ci/build/dumux-coverage
ninja coverage

# move coverage report
echo ${coverage_path}
cd ${coverage_path}
mkdir -p public
mv /dumux-ci/build/dumux-coverage/gcovr/* ./public
