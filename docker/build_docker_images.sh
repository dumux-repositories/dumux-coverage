docker build -t coverage -t git.iws.uni-stuttgart.de:4567/dumux-repositories/dumux-coverage:latest \
             --build-arg dumux_version=master \
             --build-arg dune_version=releases/2.8 \
             --build-arg subgrid_version=releases/2.8 \
             .
