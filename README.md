Creating coverage reports for DuMux
====================================

Usage
---------

Build the CMake target "coverage" in the build/dumux-coverage folder in order 
to search for .gcdo and .gcna files in dumux and in dumux-testing and create a 
coverage report in the folder gcov.

Using the docker image
-----------------------

Run the command: `docker run -it git.iws.uni-stuttgart.de:4567/dumux-repositories/dumux-coverage:latest`
Inside the container: 
* Download dumux-coverage module: `git clone https://git.iws.uni-stuttgart.de/dumux-repositories/dumux-coverage.git`)
* Run the script: `create_coverage_report`

Website
-----------

[![coverage report](https://git.iws.uni-stuttgart.de/dumux-repositories/dumux-coverage/badges/master/coverage.svg)](https://pages.iws.uni-stuttgart.de/dumux-repositories/dumux-coverage/)

Click button for displaying the latest
coverage report as a website, or [here](https://pages.iws.uni-stuttgart.de/dumux-repositories/dumux-coverage/)
